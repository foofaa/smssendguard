# SmsSendGuard for Xposed
## What?/Why?
There are many SMS blockers that focus on vetting INCOMING SMS messages, but I wanted to vet OUTGOING SMS messages. Why? Because my child prefers to let me pay for their messaging instead of going to the trouble of getting their friends to agree on a common messaging platform. I wanted to limit outgoing SMS to myself and my partner and our phone company (Vodafone). I wanted this vetting to happen no matter which SMS app my child was using.

## How?
To achieve what I wanted, I hacked up a solution using the [Xposed](http://repo.xposed.info/) framework.

This repo is only a partial Android project - you will need to follow the [Development tutorial](https://github.com/rovo89/XposedBridge/wiki/Development-tutorial), including the [Using the Xposed Framework API](https://github.com/rovo89/XposedBridge/wiki/Using-the-Xposed-Framework-API) instructions, in order to build this project.

## Code Notes
- You'll need to set up the 'vetDestinationAddress()' function to meet your needs. Note that I use the 'String.endsWith()' test to vet some numbers (so as to ignore optional number prefixes, e.g. country codes, when vetting a number).
- An obvious enhancement to this code is to build some kind of config page to specify numbers (and probably password protected to stop unauthorised access). I have not done this, but encourage any forkers to do so.
- Other links that might be of interest: [rovo89/XposedBridge](https://github.com/rovo89/XposedBridge); [Xposed Framework API](https://api.xposed.info/reference/packages.html)

