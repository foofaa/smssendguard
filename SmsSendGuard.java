package com.xayin.android;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

import java.util.ArrayList;

import android.app.PendingIntent;
import android.telephony.PhoneNumberUtils;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;

/*
 * ////////////////////////////////////////////////////////////////////////////
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * Title of work: SMS Send Guard
 * Attribute work to name: Chris Molloy
 * Attribute work to URL: https://chrismolloy.com/
 * Format of work: Software
 * ////////////////////////////////////////////////////////////////////////////
*/

@SuppressWarnings("synthetic-access")
public class SmsSendGuard implements IXposedHookZygoteInit {
	private static final String C_LOG_ID = "SmsSendGuard";
	private static final boolean C_DEBUG = true; // output debug messages to Xposed log and Android logcat

	// NB: Hooking to "android" package via IXposedHookLoadPackage / handleLoadPackage() DOES NOT WORK
	// ::: Hooking to "android" package via IXposedHookZygoteInit / initZygote() does work

	@Override
	public void initZygote(StartupParam _Param) throws Throwable {
		try {
			// void android.telephony.SmsManager.sendTextMessage(String destinationAddress, String scAddress, String text, PendingIntent sentIntent, PendingIntent deliveryIntent)
			findAndHookMethod(android.telephony.SmsManager.class, "sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, new XC_MethodHook() {
				@Override
				protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
					String sDestinationAddress = PhoneNumberUtils.stripSeparators(PhoneNumberUtils.convertKeypadLettersToDigits((String)param.args[0])).trim();
					if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: beforeHookedMethod(): Vetting '" + sDestinationAddress + "'...");
					param.args[0] = vetDestinationAddress(sDestinationAddress);
				}
			});
		}
		catch (Exception e) {
			XposedBridge.log(C_LOG_ID + "/W: handleLoadPackage(): findAndHookMethod() Exception: " + e.getLocalizedMessage());
			throw e;
		}

		try {
			// void android.telephony.SmsManager.sendMultipartTextMessage(String destinationAddress, String scAddress, ArrayList<String> parts, ArrayList<PendingIntent> sentIntents, ArrayList<PendingIntent> deliveryIntents)
			findAndHookMethod(android.telephony.SmsManager.class, "sendMultipartTextMessage", String.class, String.class, ArrayList.class, ArrayList.class, ArrayList.class, new XC_MethodHook() {
				@Override
				protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
					String sDestinationAddress = PhoneNumberUtils.stripSeparators(PhoneNumberUtils.convertKeypadLettersToDigits((String)param.args[0])).trim();
					if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: beforeHookedMethod(): Vetting '" + sDestinationAddress + "'...");
					param.args[0] = vetDestinationAddress(sDestinationAddress);
				}
			});
		}
		catch (Exception e) {
			XposedBridge.log(C_LOG_ID + "/W: handleLoadPackage(): findAndHookMethod() Exception: " + e.getLocalizedMessage());
			throw e;
		}

		if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: handleLoadPackage(): ... initialized");
	}

	// Return 'null' to knobble Send (by causing hooked method to throw an IllegalArgumentException)
	private String vetDestinationAddress(String _DestinationAddress) {
		if (_DestinationAddress.length() == 3) { // (Vodafone) service numbers, e.g. 777
			if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: vetDestinationAddress(): '" + _DestinationAddress + "' allowed");
			return _DestinationAddress;
		} else if (_DestinationAddress.equalsIgnoreCase("7233")) { // Vodafone Safe TXT service
			if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: vetDestinationAddress(): '" + _DestinationAddress + "' allowed");
			return _DestinationAddress;
		} else if (_DestinationAddress.endsWith("654321")) {
			if (C_DEBUG) XposedBridge.log(C_LOG_ID + "/D: vetDestinationAddress(): '" + _DestinationAddress + "' allowed");
			return _DestinationAddress;
		} else {
			XposedBridge.log(C_LOG_ID + "/W: vetDestinationAddress(): '" + _DestinationAddress + "' DISALLOWED");
			return null;
		}
	}

}
